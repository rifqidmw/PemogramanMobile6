package com.gmail.darmawan.rifqi.pmobile6;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Movie> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MoviesAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAdapter = new MoviesAdapter(movieList);
        RecyclerView.LayoutManager mLayoutManager =
                new LinearLayoutManager
                        (getApplicationContext());
        recyclerView = (RecyclerView)
                findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(mLayoutManager);recyclerView.setAdapter(mAdapter);
        prepareMovieData();
    }
    private void prepareMovieData() {
        Movie movie = new Movie("Mad Max: Fury Road", "Action & Adventure", "2015", "9.5", R.drawable.madmax);
                movieList.add(movie);
        movie = new Movie("Inside Out", "Animation, Kids & Family", "2015", "8.0", R.drawable.insideout);
                movieList.add(movie);
        movie = new Movie("Star Wars: Episode VII - The Force Awakens", "Action", "2015", "9.1", R.drawable.starswar);
                movieList.add(movie);
        movie = new Movie("Shaun the Sheep", "Animation", "2002", "6.8", R.drawable.shaun);
                movieList.add(movie);
        movie = new Movie("The Martian", "Science Fiction & Fantasy", "2015", "10.0", R.drawable.themartian);
                movieList.add(movie);
        movie = new Movie("Mission: Impossible Rogue Nation", "Action", "2015", "8.2", R.drawable.mission);
                movieList.add(movie);
        movie = new Movie("Up", "Animation", "2009", "8.8", R.drawable.up);
        movieList.add(movie);
        movie = new Movie("Star Trek", "Science Fiction", "2009", "8.9", R.drawable.startrek);
        movieList.add(movie);
        movie = new Movie("The LEGO Movie", "Animation", "2008", "7.0", R.drawable.lego);
                movieList.add(movie);
        movie = new Movie("Iron Man", "Action & Adventure", "2014", "8.9", R.drawable.iron);
                movieList.add(movie);
        movie = new Movie("Aliens", "Science Fiction","1986", "6.7", R.drawable.aliens);
        movieList.add(movie);
        movie = new Movie("Chicken Run", "Animation", "2000", "6.6", R.drawable.chicken);
                movieList.add(movie);
        movie = new Movie("Back to the Future", "Science Fiction", "1985", "9.8", R.drawable.future);
                movieList.add(movie);
        movie = new Movie("Raiders of the Lost Ark", "Action & Adventure", "1981", "7.1", R.drawable.raiders);
        movieList.add(movie);
        movie = new Movie("Goldfinger", "Action & Adventure", "1965", "7.7", R.drawable.gold);
                movieList.add(movie);
        movie = new Movie("Guardians of the Galaxy", "Science Fiction & Fantasy", "2014", "8.1", R.drawable.guardian);
        movieList.add(movie);
        mAdapter.notifyDataSetChanged();
    }
}