package com.gmail.darmawan.rifqi.pmobile6;

import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by yolo on 11/04/18.
 */

public class MoviesAdapter extends
        RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {
    private List<Movie> moviesList;
    public class MyViewHolder extends
            RecyclerView.ViewHolder {
        public TextView title, year, genre, rating;
        public ImageView img;
        public MyViewHolder(View view) {
            super(view);
            img = (ImageView) view.findViewById(R.id.img);
            title = (TextView)
                    view.findViewById(R.id.title);
            genre = (TextView)
                    view.findViewById(R.id.genre);
            year = (TextView) view.findViewById(R.id.year);
            rating = (TextView) view.findViewById(R.id.rating);
        }
    }
    public MoviesAdapter(List<Movie> moviesList) {
        this.moviesList = moviesList;}
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup
                                                   parent, int viewType) {
        View itemView =
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.movie_item, parent,
                                false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int
            position) {
        Movie movie = moviesList.get(position);
        holder.img.setImageResource(movie.getImg());
        holder.title.setText(movie.getTitle());
        holder.genre.setText(movie.getGenre());
        holder.year.setText(movie.getYear());
        holder.rating.setText(movie.getRating());
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
