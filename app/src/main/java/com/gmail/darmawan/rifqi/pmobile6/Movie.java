package com.gmail.darmawan.rifqi.pmobile6;

import android.media.Image;
import android.widget.ImageView;

/**
 * Created by yolo on 11/04/18.
 */

public class Movie {
    private String title, genre, year, rating;
    public int img;
    public Movie() {
    }
    public Movie(String title, String genre, String year, String rating, int img) {
        this.img = img;
        this.title = title;
        this.genre = genre;
        this.year = year;
        this.rating = rating;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }public void setTitle(String name) {
        this.title = name;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getYear() {
        return year;
    }
    public void setYear(String year) {
        this.year = year;
    }
    public String getGenre() {
        return genre;
    }
    public void setGenre(String genre) {
        this.genre = genre;
    }
}
